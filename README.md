# Data
Contains the Data for the [weyerguet](https://gitlab.com/weyerguet) project

## sel-data
Created by the [sel-data-converter](https://gitlab.com/weyerguet/sel-data-converter)

## wp-logs
Created by the [wp-data-converter](https://gitlab.com/weyerguet/wp-data-converter)

### Get raw data from Wärmepumpe

Run [wp-data-converter](https://gitlab.com/weyerguet/wp-data-converter) in the same network as the WP.

OR do it manually:

Doku: https://dimplex.atlassian.net/wiki/spaces/DW/pages/2862383109/NWPM
1. Connect to network of the WP 
2. In finder/file-explorer go to `ftp://192.168.1.162/usr/local/www/flash/http/log/`
where `192.168.1.162` is the IP-address of the wp. It might be something different 
3. Default username: httpadmin password: fhttpadmin
4. Copy all logfiles to local computer (they are .gz files) into the `data/wp-logs/raw` folder
5. Run `bash data/wp-logs/unpack_gz.sh` to convert (uncompress) the .gz files into .csv files
6. Commit the new files: `git push . && git commit -m "new wp-data added" && git push`