#!/bin/bash

# Uncompresses all files in the subfolders
# -r : Recursively
# -d : Decompress
# -f : force (overwrite existing files)
# -v : verbose
gzip -r -d -f -v $PWD